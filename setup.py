from setuptools import setup, find_packages


setup(
    name='mkdocs-numbered-admonitions-plugin',
    version='1.0.0',
    description='A MkDocs plugin',
    long_description='',
    keywords='mkdocs',
    url='',
    author='Benoît Domart',
    author_email='your email',
    license='MIT',
    python_requires='>=3.7',
    install_requires=[
        'mkdocs'
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    packages=find_packages(),
    entry_points={
        'mkdocs.plugins': [
            'numbered-admonitions=mkdocs_numbered_admonitions_plugin.plugin:NumberedAdmonitionsPlugin'
        ]
    }
)
