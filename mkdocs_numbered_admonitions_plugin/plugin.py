from mkdocs import utils as mkdocs_utils
from mkdocs.config import config_options
from mkdocs.config.base import Config
from mkdocs.plugins import BasePlugin


class NumberedAdmonitionsPluginConfig(Config):
    # Récupération des différents paramètres.
    start_characters = config_options.Type(str, default='!!!')
    expandable_start_characters = config_options.Type(str, default='???')
    admonition_types_to_number = config_options.Type(dict, default={})
    admonition_title_left = config_options.Type(str, default=' (')
    admonition_title_right = config_options.Type(str, default=')')
    shared_counter = config_options.Type(list, default=[])


class NumberedAdmonitionsPlugin(BasePlugin[NumberedAdmonitionsPluginConfig]):

    def on_config(self, config):
        # Permet d'avoir la correspondance entre :
        # - la str permettant de définir un admonition numéroté (la clef)
        # - et la str permettant de définir un admonition standard (la valeur).
        # Par défaut, les valeurs sont égales aux clefs.
        self.CORRESPONDANCE = {
            self.config.start_characters: '!!!',
            self.config.expandable_start_characters: '???',
            self.config.expandable_start_characters + '+': '???+'
        }
        # Si le paramètre a été utilisé, ce dictionnaire permet de faire le
        # lien entre les différents types d'admonition qui partagent le même
        # compteur. Pour chaque liste de types dans ce cas, on ajoute dans
        # le dictionnaire :
        # - la clef : le type courant,
        # - la valeur : le premier type de la liste (choix arbitraire).
        self.SAME_COUNTER = {}
        for same in self.config.shared_counter:
            types_list = same.replace(' ', '').split(',')
            type_ref = types_list[0]
            for type in types_list:
                self.SAME_COUNTER[type] = type_ref

    def on_page_markdown(self, markdown, page, config, files):
        # Dictionnaire permettant d'avoir le lien entre le tag et
        # le numéro de l'admonition.
        self.ADMONITION_TAGGED = {}

        # Le résultat - Le fichier modifié
        modified_lines = []

        self.admonition_type_counter = {}
        lines = markdown.split('\n')
        for line in lines:
            # On parcourt chaque ligne du fichier.
            admonition_definition = self.get_admonition_definition(line)
            if admonition_definition is None:
                # Cette ligne ne définit pas un admonition, on ne fait rien :
                modified_lines.append(line)
                continue

            # On est sur une ligne qui définit un admonition.
            # On commence par récupérer son type.
            admonition_infos = self.get_admonition_infos(line,
                                                         admonition_definition)

            # Si ce type d'admonition n'a pas été recensé dans le mkdocs.yml,
            # on ne fait rien.
            admonition_type = admonition_infos['type']
            if admonition_type not in self.config.admonition_types_to_number:
                modified_lines.append(line)
                continue

            # Si ce type d'admonition est recensé dans le mkdocs.yml,

            # on incrémente le compteur correspondant à ce type d'admonition.
            counter = self.get_counter_value(admonition_type)

            # On récupère son éventuel titre (vide par défaut) :
            admonition_title = admonition_infos['title']
            if admonition_title != '':
                # Si le titre a été renseigné, on le met entre ce qui a été
                # précisé par l'utilisateur (entre parenthèses par défaut).
                left = self.config.admonition_title_left
                right = self.config.admonition_title_right
                admonition_title = f'{left}{admonition_title}{right}'

            # On récupère les potentiels espaces et tabs en début de ligne.
            left_blank = self.get_left_blank(line)

            # On récupère l'éventuel "tag" associé à cet admonition
            # (vide par défaut) :
            if 'tag' in admonition_infos:
                # Un tag a été indiqué, on ajoute cette étiquette et la valeur
                # du compteur correspondante pour gérer les éventuelles
                # références.
                admonition_tag = admonition_infos['tag']
                self.ADMONITION_TAGGED[admonition_tag] = counter
                # On ajoute une ancre à cet endroit, avec l'étiquette précisée.
                modified_lines.append(
                    f'{left_blank}<a id="{admonition_tag}"></a>'
                )

            # On génère la nouvelle ligne correspondant à la définition de
            # cet admonition :
            modified_line = self.get_modified_line(left_blank,
                                                   admonition_definition,
                                                   admonition_type,
                                                   admonition_title,
                                                   counter)
            modified_lines.append(modified_line)

        # Toutes les lignes du fichier markdown on été traitées.
        # On "recréée" le fichier.
        modified_markdown = '\n'.join(modified_lines)

        # Gestion de références :
        for tag in self.ADMONITION_TAGGED:
            if f'@{tag}' in modified_markdown:
                # S'il y a des références vers des admonitions numérotés,
                # on rajoute un lien vers ceux-ci, en affichant, en gras,
                # la valeur du compteur qui correspond.
                counter = self.ADMONITION_TAGGED[tag]
                modified_markdown = modified_markdown.replace(
                    f'@{tag}',
                    f'<a href="#{tag}"><b>{counter}</b></a>'
                )

        return modified_markdown

    def get_admonition_definition(self, line: str) -> str:
        """
        Renvoie la suite de caractères ayant permis de définir l'admonition.
        Il y a trois possibilites, qui par défaut sont :
        - !!!
        - ???
        - ???+
        Si la ligne courante ne correspond pas à la définition d'un
        admonition, cette fonction renvoie None.
        """

        line = line.strip().replace('\t', '')
        if line.startswith(self.config.start_characters):
            return self.config.start_characters
        elif line.startswith(self.config.expandable_start_characters + '+'):
            return self.config.expandable_start_characters + '+'
        elif line.startswith(self.config.expandable_start_characters):
            return self.config.expandable_start_characters
        else:
            return None

    def get_admonition_infos(self, line, admonition_definition) -> dict:
        """
        Découpe la ligne du fichier markdown où un admonition a été défini,
        afin de récupérer toutes les informations :
        - son type (clef 'type') : info, note, ...
        - son titre (clef 'title') : vide par défaut.
        - son étiquette (clef 'tag') : vide par défaut.
        - ...
        """
        admonition_infos = {}
        # Pour cela, on supprime les caractères indiquant qu'on est en train
        # de définir un admonition,
        # et on supprime les espaces en début et en fin.
        admonition = line.replace(admonition_definition, '').strip()

        # On récupère le type de l'admonition :
        pos = 0
        while pos < len(admonition) and admonition[pos] != ' ':
            pos += 1
        admonition_infos['type'] = admonition[:pos]

        admonition = admonition[pos:]

        # On récupère son éventuel titre (vide par défaut) :
        admonition_title = ''
        if admonition != '':
            # On vérifie qu'il ne s'agit pas d'une autre option
            # (tag="..." par exemple) :
            if not admonition.split('"')[0].endswith("="):
                # Le titre a été renseigné.
                admonition_title = admonition.split('"')[1]
        admonition_infos['title'] = admonition_title
        # On n'a récupéré le titre, on l'enlève :
        admonition = admonition.replace(f'"{admonition_title}"', '').strip()

        # On enlève le dernier caractère, qui, après avoir supprimé
        # les éventuels espaces en trop, est un ".
        admonition = admonition.strip()[:-1]
        # On récupère toutes les options éventuellements indiquées :
        if '="' in admonition:
            admonition_options = dict((key.strip(), value.strip())
                                      for key, value in (element.split('="')
                                      for element in admonition.split('" ')))
            admonition_infos.update(admonition_options)
        return admonition_infos

    def get_counter_value(self, admonition_type):
        """
        Calcule la valeur du compteur en fonction du
        type de l'admonition courant.
        """
        if admonition_type in self.SAME_COUNTER:
            admonition_type = self.SAME_COUNTER[admonition_type]
        if admonition_type in self.admonition_type_counter:
            self.admonition_type_counter[admonition_type] += 1
        else:
            self.admonition_type_counter[admonition_type] = 1
        return self.admonition_type_counter[admonition_type]

    def get_left_blank(self, line: str) -> str:
        """
        Renvoie les éventuels espaces et tabulations
        présents en début de ligne.
        """
        pos = 0
        while pos < len(line) and line[pos] in ' \t':
            pos += 1
        return line[:pos]

    def get_modified_line(self,
                          left_blank,
                          admonition_definition,
                          admonition_type,
                          admonition_title,
                          counter):
        """
        Génère la nouvelle ligne, à partir de toutes les informations
        récupérées et calculées.
        """
        modified_line = ''
        # On indente correctement :
        modified_line += left_blank
        # On remplace (éventuellement) la chaîne de caractères ayant définie
        # l'admonition par celle qui correspond : !!!, ??? ou ???+
        modified_line += self.CORRESPONDANCE[admonition_definition]
        # On indique le type de l'admonition, suivi d'un espace :
        modified_line += admonition_type + ' '
        # On indique le libellé correspondant à ce type d'admonition
        # numéroté, suivi du compteur :
        label = self.config.admonition_types_to_number[admonition_type]
        label = f'"{label} {counter}'
        # et on ajoute l'éventuel titre ajouté par l'utilisateur :
        label += f'{admonition_title}"'
        return modified_line + label
