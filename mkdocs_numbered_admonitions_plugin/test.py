import plugin

test = plugin.NumberedAdmonitionsPlugin()

admonition_1 = '!!!theoreme'
admonition_2 = '!!!theoreme "avec titre"'
admonition_3 = '!!!theoreme "Pythagore" tag="pythagore"'
admonition_4 = '!!!theoreme tag="suite_arithmetique"'
admonition_5 = '!!!theoreme tag="suite_arithmetique"  autre_prop="autre valeur"'
admonition_6 = '!!! theoreme'
admonition_7 = '!!! theoreme "avec titre"'
admonition_8 = '!!! theoreme "Pythagore" tag="pythagore"'
admonition_9 = '!!! theoreme tag="suite_arithmetique"'
admonition_10 = '!!! theoreme tag="suite_arithmetique"  autre_prop="autre valeur"'

assert test.get_admonition_infos(admonition_1, '!!!') == {'type': 'theoreme', 'title': ''}
assert test.get_admonition_infos(admonition_2, '!!!') == {'type': 'theoreme', 'title': 'avec titre'}
assert test.get_admonition_infos(admonition_3, '!!!') == {'type': 'theoreme', 'title': 'Pythagore', 'tag': 'pythagore'}
assert test.get_admonition_infos(admonition_4, '!!!') == {'type': 'theoreme', 'title': '', 'tag': 'suite_arithmetique'}
assert test.get_admonition_infos(admonition_5, '!!!') == {'type': 'theoreme', 'title': '', 'tag': 'suite_arithmetique', 'autre_prop': 'autre valeur'}
assert test.get_admonition_infos(admonition_6, '!!!') == {'type': 'theoreme', 'title': ''}
assert test.get_admonition_infos(admonition_7, '!!!') == {'type': 'theoreme', 'title': 'avec titre'}
assert test.get_admonition_infos(admonition_8, '!!!') == {'type': 'theoreme', 'title': 'Pythagore', 'tag': 'pythagore'}
assert test.get_admonition_infos(admonition_9, '!!!') == {'type': 'theoreme', 'title': '', 'tag': 'suite_arithmetique'}
assert test.get_admonition_infos(admonition_10, '!!!') == {'type': 'theoreme', 'title': '', 'tag': 'suite_arithmetique', 'autre_prop': 'autre valeur'}

print('Tous les tests sont passés !')