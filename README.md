# mkdocs-numbered-admonitions-plugin

Plugin MkDocs permettant :

- d'avoir des [*admonitions*][admonitions] automatiquement numérotés,
- de faire des liens vers ces admonitions (fonctionnement calqué sur celui des commandes `\label{}` et `\ref{}` en $\LaTeX$), en affichant le compteur de l'admonition lié.

## Version

v1.0.0

## Besoin initial

Le besoin initial est de migrer des cours de maths (avec des définitions, théorèmes, propriétés, exemples, remarques ... numéroté·e·s) de $\LaTeX$ vers [Material for MkDocs][mkdocs_material].

Le but de ce plugin est donc de pouvoir écrire

```md
!!!theoreme "Pythagore"
    Soit $ABC$ un triangle. ...
```

et obtenir

![](./img/example_theorem.png)

où :

- l'ajout du libellé "Théorème" est automatique (lié ici au type d'admonition `theoreme`),
- la numérotation de chaque type d'amonitions (`theoreme`, `definition`, ...) est automatique,
- et où il est possible de faire des [références](#ajout-de-référence) (lien hypertexte et affichage du numéro - pour une impression papier) à ce bloc à un autre endroit du cours.

Un exemple d'utilisation de ce plugin est disponible [ici](https://domart.frama.io/mkdocs-numbered-admonitions-plugin-example/).

## Installation

En utilisant pip:

```
pip install git+https://framagit.org/domart/mkdocs-numbered-admonitions-plugin
```

Le plugin (`numbered_admonitions`) doit ensuite être activé dans le `mkdocs.yml`.
Il faut au minimum indiquer les types d'*admonitions* qui seront automatiquement numérotés, en indiquant le libellé qui sera affiché. Pour cela, il faut utiliser le paramètre `admonition_types_to_number`, dont la valeur est un dictionnaire dont les clefs sont les types d'admonitions concernés, et les valeurs le libellé qui sera affiché.

Les *admonitions* dont le type n'est pas listé dans le paramètre `admonition_types_to_number` ne sont pas impactés par ce plugin.

### Exemple de configuration minimale

Par exemple, la configuration :

```yaml
plugins:
    - search
    - numbered-admonitions:
        admonition_types_to_number:
            'note': 'Remarque'
            'info': 'Un autre titre'
```

fera que les *admonitions* `note` auront comme titre :

- Remarque 1
- Remarque 2
- ...

et que les *admonitions* `info` auront comme titre :

- Un autre titre 1
- Un autre titre 2
- ...

Si un titre a été précisé pour l'admonition, celui-ce sera ajouté entre parenthèses (par défaut) après le numéro ajouté automatiquement.
Par exemple,

```md
???+info "Exemple d'info"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
```

créera l'*admonition* repliable, ouvert par défaut, suivant :

![](./img/example_info_titre.png)

### Configuration optionelle

#### Les paramètres possibles

- `admonition_types_to_number` (obligatoire) : dictionnaire.
- `shared_counter` : liste.
- `admonition_title_left` : chaîne de caractères.
- `admonition_title_right` : chaîne de caractères.

#### Autre chose que des parenthèses autour du titre

Lorsqu'un titre a été ajouté à l'admonition, celui-ci est ajouté entre parenthèses après le numéro. Il est possible de modifier ce choix des parenthèses.

Par exemple, avec la configuration

```yaml
plugins:
    - search
    - numbered-admonitions:
        admonition_types_to_number:
            'note': 'Remarque'
            'info': 'Un autre titre'
        admonition_title_left: ' - '
        admonition_title_right: ''
```

```md
???+info "Exemple d'info"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
```

ajoutera l'admonition repliable (ouvert par défaut), suivant :

![](./img/example_info_titre_2.png)


#### Plusieurs types partageant le même compteur

On peut également souhaiter avoir plusieurs types d'*admonition* partageant le même compteur.

Par exemple, on peut souhaiter avoir des blocs "Remarque" (au singulier) et des blocs "Remarques" (au pluriel) partageant la même numérotation.

Le paramètre `shared_counter` permet de faire cela. La configuration suivante :

```yaml
plugins:
    - search
    - numbered-admonitions:
        admonition_types_to_number:
            'theoreme': 'Théorème'
            'definition': 'Définition'
            'exemple': 'Exemple'
            'exemples': 'Exemples'
            'remarque': 'Remarque'
            'remarques': 'Remarques'
            'propriete': "Propriété"
        shared_counter:
            - propriete, proprietes
            - remarque, remarques
```

permettra d'avoir d'une part :
- des blocs "Propriété" et "Propriétés" numérotés ensemble,
- des blocs "Remarque" et "Remarques" numérotés ensemble.

Il peut y avoir plus de 2 blocs partageant le même compteur, il suffit d'indiquer tous les types concernés, séparés par des virgules.


## Ajout de référence

Il est également possible d'ajouter des références vers un *admonition* numéroté.

Pour cela, lors de la création d'un *admonition* numéroté, il faut ajouter `tag="<l_étiquette>"` (entre double quotes) après son éventuel titre. Cela a un comportement semblable au `\ref{<l_étiquette>}` en $\LaTeX$.

Pour y faire référence (**dans le même fichier `.md`**), il suffit d'ajouter `@<l_étiquette>` dans le texte. Ceci sera remplacé par un lien hypertexte vers l'*admonition* concerné, avec l'affichage de son numéro en gras.

#### Exemples

```md
???+info tag="le_tag_de_cette_info"
```

ou

```md
???+info "Exemple d'info" tag="le_tag_de_cette_autre_info"
```

Pour y faire référence, il faut écrire `@le_tag_de_cette_info` ou `@le_tag_de_cette_autre_info` dans le texte.

!!!info Remarque

    Il est également possible d'utiliser `#le_tag_de_cette_info` de manière classique, dans la page `.md` courante ou dans une autre.

    Cela ajoutera un lien hypertexte vers l'admonition concerné, mais sans indiquer le numéro du compteur correspondant.

## Remarque

[Cette page][creation_admonitions] explique comment créer ses propres *admonitions*, en personnalisant les couleurs et les icônes.



[mkdocs_material]: https://squidfunk.github.io/mkdocs-material/
[admonitions]: https://squidfunk.github.io/mkdocs-material/reference/admonitions/
[creation_admonitions]: https://squidfunk.github.io/mkdocs-material/reference/admonitions/#custom-admonitions